<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>Document</title>
</head>

<body>
    <div class="main">
        <div class="wrapper">
            <h2 class="heading">Trắc nghiệm vui về Xuân Hiếu</h2>
            <div class="question-wrapper">
                <div class="page page-1">
                    <h3>Page 1/2</h3>
                    <div class="question-list">

                    </div>
                    <div class="next"><span>Tiếp theo</span></div>
                </div>
                <div class="page page-2" style="display: none;">
                    <h3>Page 2/2</h3>
                    <div class="question-list">

                    </div>
                    <div class="page-btn">
                        <div class="back">Quay lại trang trước</div>
                        <div class="submit-btn">Nộp bài</div>
                    </div>
                </div>
            </div>
            <div id="result" style="display:none">
            </div>
        </div>

    </div>
    <script>
        const quiz = [{
                question: "Biệt danh của Võ Xuân Hiếu là gì ?",
                choices: [
                    "Vua đệm Everon Xuanhieu",
                    "Ông chủ giayxhshop.com",
                    "Chủ tịch XH Soft",
                    "Coder chúa",
                ],
                answer: 4
            },
            {
                question: "Xuân Hiếu sở hữu mấy công ty ?",
                choices: [
                    "3",
                    "2",
                    "1",
                    "4",
                ],
                answer: 1
            },
            {
                question: "Xuân Hiếu Idol xinh năm bao nhiêu",
                choices: [
                    "2001",
                    "1999",
                    "2003",
                    "2002(Đáp án nè)",
                ],
                answer: 4
            },
            {
                question: "GPA của Xuân Híu là bao nhiểu ?",
                choices: [
                    "3.84",
                    "3.72",
                    "3.96",
                    "4.0",
                ],
                answer: 2
            },
            {
                question: "Xuân Hiếu sinh ra ở đâu",
                choices: [
                    "Hà Nội",
                    "Nghệ An",
                    "Sài Gòn",
                    "Đà Nẵng",
                ],
                answer: 1
            },

            {
                question: "Xuân Hiếu ghét ai nhất ?",
                choices: [
                    "Hoàng Nguyễn",
                    "Hoàng Nguyễn Duy",
                    "Nguyễn Duy Hoàng",
                    "Duy Hoàng",
                ],
                answer: 3
            },
            {
                question: "Số áo của Xuân Hiếu là ?",
                choices: [
                    "22",
                    "33",
                    "44",
                    "17",
                ],
                answer: 2
            },
            {
                question: "Xuân Hiếu sinh ngày bao nhiêu",
                choices: [
                    "68",
                    "72",
                    "22",
                    "11",
                ],
                answer: 3
            },
            {
                question: "Tài khoản ngân hàng của xuân hiếu có mấy chữ số ?",
                choices: [
                    "9 chữ số",
                    "10 chữ số",
                    "8 chữ số",
                    "12 chữ số",
                ],
                answer: 1
            },
            {
                question: "Thương hiệu Everon xuân hiếu tọa lạc ở đâu ?",
                choices: [
                    "Nguyễn Trãi , Thanh Xuân",
                    "TP Vinh , Nghệ An",
                    "Học viện tài chính",
                    "Khoa Hóa, ĐHKHTN ",
                ],
                answer: 4
            },

        ]
        renderQuestion(quiz);

        var nextPageBtn = document.querySelector('.next');
        var backBtn = document.querySelector('.back');
        var submitBtn = document.querySelector('.submit-btn');
        var page1 = document.querySelector('.page-1');
        var page2 = document.querySelector('.page-2');
        var answers = document.getElementsByName("answer")
        var answerForms = document.querySelectorAll('.answerForm');
        var result = document.getElementById('result');
        var score = 0;
        let userChoices = [];
        let choice = '';
        const badScore = "Bạn quá kém, cần ôn tập thêm"
        const mediumScore = "Cũng bình thường"
        const goodScore = "Sắp sửa làm được trợ giảng lớp PHP"

        console.log(4142132314)
        // Đưa ra kết quả và nhận xét
        function checkResult(score) {
            if (score < 4) {
                result.innerHTML += '<span class="comment" style="color:#dc3545 ;">' + badScore + '</span>';
            } else if (score >= 4 && score <= 7) {
                result.innerHTML += '<span class="comment" style="color: #ffc107;">' + mediumScore + '</span>';
            } else if (score > 7) {
                result.innerHTML += '<span class="comment" style="color:#28a745 ;">' + goodScore + '</span>';
            }
            document.querySelector('#result').innerHTML += '<a href="/index.php" class="btn-retry">Làm lại bài trắc nghiệm</a>'
        }

        // Tính điểm
        answerForms.forEach((answerForm, index) => {

            answerForm.onchange = function() {
                // 4142132314
                for (let i = 0; i < answerForm.elements.length; i++) {
                    const element = answerForm.elements[i];

                    if (element.checked == true) {
                        choice = Number(element.value) + 1;
                        // console.log(choice)
                        userChoices[index] = choice;
                    }

                }

            }

        })

        //Tính điểm 
        function checkAnswer(userChoices) {
            userChoices.forEach((userChoice, index) => {
                if (userChoice === Number(quiz[index].answer)) {
                    score++;
                }
            })
        }

        // Render mỗi page 5 câu hỏi
        function renderPage(i, data, index) {
            var page = document.querySelector(`.page-${i} .question-list`);
            var ask = ` <div class="ask">
                            <p class="ask-text">
                                <span class="question-order">Câu hỏi ${index+1}: ${data.question }</span>
                                
                            </p>
                        </div>`
            var answers = ``
            data.choices.forEach((choice, num) => {
                answers += `<div class="input-group">
                                    <input type="radio" name="answer" value="${num}" id="answer_1">
                                    <label class="form-label">${choice}</label>
                                </div>`
            })
            page.innerHTML += `<div class="question">` +
                ask +
                `<div class="answer">
                                        <form action="" class="answerForm">` +
                answers +
                `</form>
                                    </div>
                                </div>`


        }

        function renderQuestion(quiz) {
            quiz.forEach((question, index) => {

                if (index <= 4) {
                    renderPage(1, question, index)
                } else renderPage(2, question, index)

            })
        }


        // Xử lý khi ấn nộp bài
        submitBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'none';
            result.style.display = 'flex';
            checkAnswer(userChoices)
            result.innerHTML = "<span>Score: " + score + "/" + quiz.length + "</span>";
            checkResult(score)
        }

        // Xử lý khi ấn trang tiếp theo
        nextPageBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'block';
            window.scrollTo({
                top: 100,
                behavior: 'smooth'
            });

            console.log('to page 2')
        }

        // Xử lý khi ấn quay lại trang trước
        backBtn.onclick = function() {
            page2.style.display = 'none';
            page1.style.display = 'block';
            window.scrollTo({
                top: 100,
                behavior: 'smooth'
            });

        }
    </script>
</body>

</html>